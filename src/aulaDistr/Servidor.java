package aulaDistr;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {

	public static void main(String[] args) {
		try {
			System.out.println("Servidor Ligado");
			ServerSocket servidor = new ServerSocket(8000);
			Socket s = servidor.accept();
			DataInputStream dado = new DataInputStream(s.getInputStream());
			String info = dado.readUTF();
			System.out.println(info);
			s.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
