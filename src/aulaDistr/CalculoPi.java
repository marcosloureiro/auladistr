package aulaDistr;

public class CalculoPi {	
	private double pi;
	private int n;
	
	public CalculoPi(int n){
		this.n = n;
		this.pi = 2.0;
	}
	public double calculo() {
		for(int i=1; i<=n; i++) {
			pi = pi * (2*i)*(2*i)/((2*i)-1)/((2*i)+1);
		}
		return pi;
	}
}
